import React from 'react';
import { Spin, Icon } from 'antd';

const Loading = ({ size = 24, tip }) => (
  <div>
    <Spin tip={tip} indicator={<Icon type="loading" style={{ fontSize: size }} spin />} />
  </div>
)

export default Loading
import React, { useState } from 'react';
import { Modal, Input } from 'antd';

const ConfirmationModal = ({ title, visible, confirmationButtonText, confirmationText, onConfirm, onCancel }) => {

  const [ text, setText ] = useState('')

  return (
    <Modal
      title={title}
      visible={visible}
      onOk={onConfirm}
      onCancel={onCancel}
      okText={confirmationButtonText}
      okButtonProps={{ disabled: text !== confirmationText }}
      cancelText="Cancel"
      okType="danger"
    >
      <p>Please confirm by typing "{confirmationText}"</p>
      <Input value={text} onChange={e => setText(e.target.value)} placeholder="Confirmation" /><br></br>
    </Modal>
)
}

export default ConfirmationModal
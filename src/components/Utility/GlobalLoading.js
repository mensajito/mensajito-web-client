import React, { useState } from 'react';
import { Layout } from 'antd';
import EmptyHeader from '../Admin/EmptyHeader'
import EmptySidebar from '../Admin/EmptySidebar'
import Loading from './Loading'
import '../Admin/Sidebar.css'

const Admin = (props) => {

  const [collapsed, setCollapsed] = useState(false)

  const contentClassNames =  collapsed
    ? 'sidebar--collapsed absolute t-0 r-0 sidebar'
    : 'sidebar absolute t-0 r-0 '

  return (
    <Layout>
      <EmptySidebar
        collapsed={collapsed}
        setCollapsed={setCollapsed} />
      <div className={contentClassNames}>
        <EmptyHeader />
        <main className="h-full w-full flex items-center justify-center relative p-4 pb-16">
          <Loading tip="Loading Account..." size={48} />
        </main>
      </div>
    </Layout>
    )
  }

export default Admin
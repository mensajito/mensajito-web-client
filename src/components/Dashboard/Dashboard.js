import React from 'react';
import { Card, Avatar, Icon, Tag, Input } from 'antd';
import styled from 'styled-components'

const { Meta } = Card;
const { Search } = Input;

const Grid = styled.div.attrs({
  className: 'w-full'
})`
  display: grid;
  grid-gap: 10px;
  grid-template-columns: repeat(3, 1fr);
`

const DashboardCard = (
  <Card
    className="w-full"
    actions={[<Icon type="setting" />, <Icon type="edit" />, <Icon type="ellipsis" />]}
  >
  <Meta
    avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
    title="ECU: #2364356"
    description={<><p>Honda '95 Camry</p><Tag color="orange">Pending</Tag></>}
  />
  </Card>
)

const LastMessage = (
  <Card
    className="w-full"
    actions={[<Icon type="read" />, <Icon type="message" />]}
  >
  <Meta
    avatar={<Avatar style={{ backgroundColor: '#f56a00' }} icon="user" />}
    title="Subj: Honda"
    description={<><p>Hey that sounds great to me!</p></>}
  />
  </Card>
)

const SearchTuner = (
  <Card
    style={{gridColumn: 'span 2'}}
    title={<h1 className="flex items-center"><Icon className="mr-4" type="search"/> Explore</h1>}
    className="w-full"
  >
    <span>Search Tuner</span><br/>
    <Search placeholder="Search Tuner" onSearch={value => console.log(value)} enterButton />
  </Card>
)

const AddNewEcu = (
  <Card className="flex flex-col item-center justify-center h-full w-full">
    <article className="flex flex-col item-center justify-center h-full w-full text-xl">
      <p className="text-center">Add New ECU</p>
      <Icon type="plus-circle" />
    </article>
  </Card>
)

const Dashboard = () => {
  return (
    <Grid>
      {DashboardCard}
      {LastMessage}
      {AddNewEcu}
      {SearchTuner}
    </Grid>
  )
}

export default Dashboard
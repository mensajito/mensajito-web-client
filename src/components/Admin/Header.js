import React, { memo, useState } from 'react';
import styled from 'styled-components';
import { useAuth0 } from '../Auth/Auth';
import { Icon, Menu, Dropdown, Avatar, Badge, notification } from 'antd';
import { useTranslation } from 'react-i18next';

const languageMap = {
  es: 'español',
  en: 'english'
}

const Header = styled.header.attrs({
  className: 'w-full flex justify-end items-center'
})`
  z-index: 1000;
  position: relative;
  height: 54px;
  padding: 0;
  background: #fff;
  box-shadow: 0 1px 4px rgba(0,21,41,.08);
`

const Title = styled.h2.attrs({
  className: 'ml-4'
})`
`

const HeaderRight = styled.nav`
  margin-left:auto;
  display:inline-flex;

  & > i, & > span {
    margin: 0 10px;
  }
`

const initialMessages = [{ description: 'Here is the first.' }, { description: 'Welp, here comes the second!' }]

const DashboardHeader = ({ title }) => {

  const { logout, loginWithRedirect, isAuthenticated, user } = useAuth0();
  const { nickname, picture } = user

  const [messages, setMessages] = useState(initialMessages);
  const [language, setLanguage] = useState('español');

  const { i18n } = useTranslation()

  const openNotifications = () => {

    messages.forEach(message => {
      notification.open({
        message: message.description,
        description:
          'This is the content of the notification. This is the content of the notification. This is the content of the notification.',
        onClick: () => {
          console.log('Notification Clicked!');
        },
      });
    })

    setMessages([])
  };

  const handleLanguageMenuClick = ({ key }) => {
    setLanguage(languageMap[key])
    i18n.changeLanguage(key)
  }

  const menu = (
    <Menu>
      <Menu.Item>
        <Icon type="setting"/> Account Settings
      </Menu.Item>
      <Menu.Item>
        <Icon type="user"/> Account Center
      </Menu.Item>
      <Menu.Divider />
      {
        !isAuthenticated
        ? (<Menu.Item onClick={() => loginWithRedirect()}>
            <Icon type="login" /> Login
          </Menu.Item>)
        : (<Menu.Item onClick={() => logout()}>
        <Icon type="logout" /> Logout
      </Menu.Item>)
      }
    </Menu>
  );

  const languageMenu = (
    <Menu onClick={handleLanguageMenuClick}>
      <Menu.Item key="es">Español</Menu.Item>
      <Menu.Item key="en">English</Menu.Item>
    </Menu>
  );

  return (
    <Header className="cursor-pointer">
      <Title>{title}</Title>
      <HeaderRight>
        <Dropdown className="ml-4 mr-4" overlay={languageMenu}>
          <span className="capitalize">{language}</span>
        </Dropdown>
        <Badge onClick={() => openNotifications()} count={messages.length}>
          <Icon type="bell" />
        </Badge>
        <Dropdown className="ml-4 mr-4" overlay={menu}>
          <a className="flex items-center space-around" href="#">
          <Avatar
            src={picture}
            style={{ backgroundColor: '#87d068', marginRight: 5 }}
            size="small"
            icon="user" />
          <span>{nickname}</span>
          </a>
        </Dropdown>
      </HeaderRight>
    </Header>
  )
}

export default memo(DashboardHeader);
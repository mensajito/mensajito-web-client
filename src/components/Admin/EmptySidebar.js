import React from 'react';
import { Layout, Menu, Icon } from 'antd';
import styled from 'styled-components'

const { Sider } = Layout;
const { SubMenu } = Menu;

const RightIcons = styled.div.attrs({
  className: 'absolute right-0 top-0 h-full flex items-center'
})`
  opacity: .3;

  &:hover {
    opacity: 1;
  }
`

const MenuItem = styled(Menu.Item).attrs({
  className: 'items-center relative'
})`
& > a {
  display: flex;
  align-items:center;
}`

const SidebarComponent = ({ collapsed, setCollapsed, pathname }) => (
  <Sider
    style={{
      overflow: 'auto',
      height: '100vh',
      position: 'fixed',
      left: 0,
      boxShadow: '2px 0 6px rgba(0,21,41,.35)'}}
    collapsible collapsed={collapsed}
    onCollapse={() => setCollapsed(!collapsed)}>
    <h1 className="pointer blue text-2xl mb-0 text-white" style={{height: 56}}>
      <p className="flex justify-center items-center w-full h-full text-white" to="/">
        <Icon type="sliders" theme="filled" />
        <span>UnityTuning</span>
      </p>
    </h1>
  </Sider>
);

export default SidebarComponent;

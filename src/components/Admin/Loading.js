import React, { Component, createContext } from 'react';
import LoadingSpinner from '../Utility/Loading'

export const LoadingContext = createContext();
export const { Provider, Consumer } = LoadingContext

class Loading extends Component {

  state = {
    loading: false,
    setLoading: isLoading => this.setState({ loading: isLoading })
  }

  render () {

    const { children } = this.props

    return (
      <Provider
        value={this.state}
      >
        <div>
          { this.state.loading ?
            <div className="absolute left-0 w-full flex items-center justify-center" style={{
              zIndex: 100,
              height: '100vh',
              overflow: 'hidden',
              top: 0,
              position: 'fixed',
              backgroundColor: 'rgba(255, 255, 255, 0.7)'
            }}>
              <LoadingSpinner size={48} />
              </div>
            : null
          }
          {children}
        </div>
      </Provider>
    );
  }
}

export default Loading
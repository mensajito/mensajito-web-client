import React, { useState, useEffect, Suspense } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { Layout } from 'antd';
// import Loading from './Loading'
import { useAuth0 } from '../Auth/Auth'
import ApiWrapper from '../../helpers/api-wrapper';
import Sidebar from './Sidebar'
import Header from './Header'
import Estacion from '../Estacion/Estacion'
import Loading from './Loading'
import './Sidebar.css'

const Admin = (
  { match, location: { pathname },
  isLoading
}) => {

  const [collapsed, setCollapsed] = useState(false)
  const [isLoadingApiWrapper, setIsLoadingApiWrapper] = useState(true)

  const { token, permissions } = useAuth0();

  useEffect(() => {
    ApiWrapper.setToken(token)
    setIsLoadingApiWrapper(false)
  }, []);

  if (isLoadingApiWrapper || isLoading) return <Loading />

  const contentClassNames =  collapsed
    ? 'sidebar--collapsed absolute t-0 r-0 sidebar'
    : 'sidebar absolute t-0 r-0 '

  return (
    <Suspense fallback="loading">
      <Layout>
        <Sidebar
          pathname={pathname}
          collapsed={collapsed}
          setCollapsed={setCollapsed} />
        <div className={contentClassNames}>
          <Header title={'Estacion'} />
          <main className="w-full relative p-4">
            <Loading>
              <Switch>
                <Route path={`${match.path}estacion`} exact render={(props) => <Estacion {...props} permissions={permissions} />} />
                <Route path={`${match.path}estacion/:estacionId`} exact component={Estacion} />
                <Redirect to="/estacion"/>
              </Switch>
            </Loading>
          </main>
        </div>
      </Layout>
    </Suspense>
    )
  }

export default Admin
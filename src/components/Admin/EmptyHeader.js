import React, { memo } from 'react';
import styled from 'styled-components';
import { Icon, Menu, Dropdown, Avatar, Badge } from 'antd';

const Header = styled.header.attrs({
  className: 'w-full flex justify-end items-center'
})`
  position: relative;
  height: 54px;
  padding: 0;
  background: #fff;
  box-shadow: 0 1px 4px rgba(0,21,41,.08);

  & > i, & > span {
    margin: 0 10px;
    font-size: 18px;
  }
`

const DashboardHeader = () => {

  const menu = (
    <Menu>
      <Menu.Item>
        <Icon type="setting"/> Account Settings
      </Menu.Item>
      <Menu.Item>
        <Icon type="user"/> Account Center
      </Menu.Item>
      <Menu.Divider />
    </Menu>
  );

  return (
    <Header className="cursor-pointer">
      <Icon type="search" />
      <Icon type="question-circle" />
      <Badge>
        <Icon type="bell" />
      </Badge>
      <a className="flex items-center space-around ml-4 mr-4" href="#">
        <Avatar style={{ backgroundColor: '#87d068', marginRight: 5 }} size="small" icon="user" />
        <span>User</span>
      </a>
    </Header>
  )
}

export default memo(DashboardHeader);
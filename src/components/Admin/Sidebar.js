import React from 'react';
import { Link } from 'react-router-dom'
import { Layout, Menu, Icon } from 'antd';
import styled from 'styled-components'
import { useTranslation } from 'react-i18next';

const { Sider } = Layout;
const { SubMenu } = Menu;

const MenuItemText = styled.span.attrs({
  className: 'capitalize'
})``

const MenuItem = styled(Menu.Item).attrs({
  className: 'items-center relative'
})`
& > a {
  display: flex;
  align-items:center;
}`

const SidebarComponent = ({ collapsed, setCollapsed, pathname }) => {
  const { t } = useTranslation();

  return (
    <Sider
      style={{
        overflow: 'auto',
        height: '100vh',
        position: 'fixed',
        left: 0,
        boxShadow: '2px 0 6px rgba(0,21,41,.35)'}}
      collapsible collapsed={collapsed}
      onCollapse={() => setCollapsed(!collapsed)}>
      <h1 className="pointer blue text-2xl mb-0 text-white" style={{height: 56}}>
        <Link className="flex justify-center items-center w-full h-full text-white" to="/">
          {/* <Icon type="sliders" theme="filled" /> */}
          { collapsed ? '' : <span>Mensajito</span> }
        </Link>
      </h1>
      <Menu theme="dark" defaultSelectedKeys={[pathname]} mode="inline">
        <MenuItem key="/station">
          <Link to="/station">
            <Icon type="database" className="relative"/>
            <MenuItemText>{t('station')}</MenuItemText>
          </Link>
        </MenuItem>
        <MenuItem key="/programs">
          <Link to="/programs">
            <Icon type="apartment" className="relative"/>
            <MenuItemText>{t('programs')}</MenuItemText>
          </Link>
        </MenuItem>
        <MenuItem key="/mensajito">
          <Link to="/mensajito">
            <Icon type="cluster" className="relative"/>
            <MenuItemText>{t('mensajito')}</MenuItemText>
          </Link>
        </MenuItem>
        <MenuItem key="/user-management">
          <Link to="/user-management">
            <Icon type="team" className="relative"/>
            <MenuItemText>{t('user management')}</MenuItemText>
          </Link>
        </MenuItem>
        <SubMenu
          key="/account"
          title={
            <span className="flex items-center">
              <Icon type="setting" />
              <MenuItemText>{t('account')}</MenuItemText>
            </span>
          }
        >
          <Menu.Item key="/account/info">
            <Link to="/account/info">
              <span className="flex items-center">
                <Icon type="form" />
                <MenuItemText>{t('info')}</MenuItemText>
              </span>
            </Link>
          </Menu.Item>
          <Menu.Item key="/account/billing">
            <Link to="/account/billing">
              <span className="flex items-center">
                <Icon type="credit-card" />
                <MenuItemText>{t('billing')}</MenuItemText>
              </span>
            </Link>
          </Menu.Item>
          <Menu.Item key="/account/settings">
            <Link to="/account/settings">
              <span className="flex items-center">
                <Icon type="home" />
                <MenuItemText>{t('settings')}</MenuItemText>
              </span>
            </Link>
          </Menu.Item>
        </SubMenu>
      </Menu>
    </Sider>
  );
}

export default SidebarComponent;

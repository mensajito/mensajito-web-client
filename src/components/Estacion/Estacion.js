import React, { useState, useEffect } from 'react';
import axios from 'axios';
import styled from 'styled-components';
import ApiWrapper from '../../helpers/api-wrapper';
import { useAuth0 } from '../Auth/Auth'

const Estacion = () => {
  const [estacion, setEstacion] = useState(null)
  const { user } = useAuth0();

  console.log('user: ', user)

  const estacionId = user['https://app.mensajtio.mx/user_metadata'].estacionId

  useEffect(() => {
    const effect = async () => {
      try {
        const response = await ApiWrapper.getEstacion(estacionId)

        console.log('response: ', response)
      } catch (err) {
        console.log('error: ', err)
      }
    }
    effect()
  }, [])

  return (
    <div>Estacion</div>
  )
}

export default Estacion
import React from 'react';
import { Carousel } from 'antd';
import styled from 'styled-components';
import 'css-doodle';

const START_GRADIENT_COLOR = '#001529'
const END_GRADIENT_COLOR = '#002140'
const END_GRADIENT_COLOR_RGB = '0,33,64'

const mazeDoodle = `
:doodle {
  @grid: 68 / 70vmax;
  grid: 1px;
}
:container {
  transform: rotate(45deg) scale(1.5);
}
@random { border-top: 1px solid ${END_GRADIENT_COLOR}; }
@random { border-left: 1px solid ${END_GRADIENT_COLOR}; }
@random(.2) {
  :after {
    content: '';
    background: hsl(@rand(360), 60%, 70%);
    @size: @rand(3px);
  }
}
`

const shatteredDoodle = `
:doodle {
  @grid:4 / 70vmax;
  grid: 1px;
}
background: rgba(${END_GRADIENT_COLOR_RGB}, @rand(.9));
transition: .2s ease @rand(200ms);
transform: rotate(@rand(360deg));
clip-path: polygon(
  @rand(100%) 0, 100% @rand(100%), 0 @rand(100%)
);
`

const Wrapper = styled.main.attrs({
  className: 'relative flex h-screen w-screen overflow-hidden'
})``

const DescriptionWrapper = styled.div.attrs({
  className: 'flex-col align-center justify-center relative bg-green-900 h-full text-center'
})`
  flex: 1 0 auto;
  width:35%;
  z-index:10;

  & > .ant-carousel {
    height: 100%;
  }
`

const CarouselWrapper = styled(Carousel).attrs({
  className: 'h-full text-white'
})`
  background: linear-gradient(to right, ${START_GRADIENT_COLOR} 0%,${END_GRADIENT_COLOR} 100%);
`

const CarouselPanel = styled.article.attrs({
  className: 'flex flex-col justify-center items-center h-screen'
})``

const CarouselPanelHeader = styled.header.attrs({
  className: 'text-white w-3/5'
})``

const CarouselPanelTitle = styled.h1.attrs({
  className: 'text-white text-lg mb-2'
})``

const CarouselPanelSubText = styled.p.attrs({
  className: 'text-white text-sm'
})`
  font-size:.8rem;
  font-weight:100;
`

const LoginSection = styled.section.attrs({
  className: 'relative inline-flex h-full justify-center align-center'
})`
  flex: 2 0 auto;
`

const Content = styled.section.attrs({
  className: 'absolute '
})`
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%)
`

const Layout = ({ children }) => (
  <Wrapper>
    <DescriptionWrapper>
      <CarouselWrapper>
        <div>
          <CarouselPanel>
            <CarouselPanelHeader>
              <CarouselPanelTitle>Instant Professional Care</CarouselPanelTitle>
              <CarouselPanelSubText>
                Easily search the top tuners in the industry and start a conversation instantly.
                {/* Discuss your specific needs with our tuners and shop around with the ease of a click. */}
              </CarouselPanelSubText>
            </CarouselPanelHeader>
          </CarouselPanel>
        </div>
        <div>
          <CarouselPanel>
            <CarouselPanelHeader>
              <CarouselPanelTitle>Instant Professional Care</CarouselPanelTitle>
              <CarouselPanelSubText>
                Easily search the top tuners in the industry and start a conversation instantly.
                {/* Discuss your specific needs with our tuners and shop around with the ease of a click. */}
              </CarouselPanelSubText>
            </CarouselPanelHeader>
          </CarouselPanel>
        </div>
      </CarouselWrapper>
    </DescriptionWrapper>
    <LoginSection>
      <css-doodle>{mazeDoodle}</css-doodle>
      <Content>
        {children}
      </Content>
    </LoginSection>
  </Wrapper>
)

export default Layout
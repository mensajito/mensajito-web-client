/* eslint-disable react-hooks/exhaustive-deps */
import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { useAuth0 } from './Auth.js';

const AuthRoute = ({ component: Component, ...rest }) => {

  const { isAuthenticated } = useAuth0();

  return (
    <Route {...rest} render={(props) => (
      isAuthenticated === true
        ? <Component {...props} />
        : <Redirect to='/login' />
    )} />
  )
}

export default AuthRoute;
import React, { useEffect } from 'react';
import Auth0Lock from 'auth0-lock';
import { useAuth0 } from '../Auth/Auth'

const END_GRADIENT_COLOR = '#002140'

const Lock = () => {
  const { handleLoginResponse } = useAuth0();

  const lock = new Auth0Lock(process.env.REACT_APP_AUTH0_CLIENT_ID, process.env.REACT_APP_AUTH0_DOMAIN, {
    auth: {
      responseType: 'token id_token',
      audience: process.env.REACT_APP_AUTH0_AUDIENCE
    },
    autoParseHash: false,
    redirect: false,
    languageDictionary: {
      title: "Login"
    },
    container: 'lock-container',
    theme: {
      primaryColor: END_GRADIENT_COLOR
    }
  });

  useEffect(() => {

    // Avoid showing Lock when hash is parsed.
    if ( !(/access_token|id_token|error/.test(window.location.href)) ) {
      lock.show();
    }

    lock.on('authenticated', (authResult) => {
      const { accessToken, idTokenPayload } = authResult;

      handleLoginResponse(accessToken, idTokenPayload)
    });
  }, [])

  return (
    <div id="lock-container"></div>
  )
}

export default Lock;
/* eslint-disable react-hooks/exhaustive-deps */
import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { useAuth0 } from './Auth.js';

const PrivateRoute = ({ component: Component, ...rest }) => {

  const { isAuthenticated } = useAuth0();

  return (
    <Route {...rest} render={(props) => (
      isAuthenticated === false
        ? <Component {...props} />
        : <Redirect to='/' />
    )} />
  )
}

export default PrivateRoute;
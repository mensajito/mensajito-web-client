import React, { useState, useEffect, createContext, useContext } from 'react';
import jwtDecode from 'jwt-decode';
import { isString, isNil, isObject } from 'lodash';

export const Auth0Context = createContext();
export const useAuth0 = () => useContext(Auth0Context);

export const Auth0Provider = ({
  children,
  ...initOptions
}) => {
  const [isAuthenticated, setIsAuthenticated] = useState();
  const [user, setUser] = useState();
  const [token, setToken] = useState();
  const [decodedToken, setDecodedToken] = useState();
  const [permissions, setPermissions] = useState();
  const [loading, setLoading] = useState(true);

  // TODO: refactor how token/decoded get fetched and use async
  useEffect(() => {
    const initAuth0 = async () => {
      const isAuthenticated = await checkIfIsAuthenticated();

      setIsAuthenticated(isAuthenticated);

      if (isAuthenticated) {
        const user = await getPersistedUser();
        const token = await getPersistedToken();
        const decodedToken = await getPersistedDecodedToken();

        setPersistedUser(user);
        setToken(token);
        setPermissions(decodedToken.permissions);
      }

      setLoading(false);
    };
    initAuth0();
    // eslint-disable-next-line
  }, []);

  const checkIfIsAuthenticated = async () => {
    const decodedToken = await getPersistedDecodedToken()

    if (isNil(decodedToken) || Date.now() >= decodedToken.exp * 1000) {
      logout()

      return false
    }

    setDecodedToken(decodedToken)
    return true
  }

  // user can be null or stringified value
  const getPersistedUser = async () => {
    const user = await localStorage.getItem('authUser')

    if (isString(user)) {
      return JSON.parse(user)
    }

    return user
  }

  const setPersistedUser = async (user) => {

    if (isObject(user)) {
      // extrapolate user id from user.sub
      user = Object.assign({}, user, {
        id: user.sub.split('|')[1]
      })

      setUser(user)

      return localStorage.setItem('authUser', JSON.stringify(user))
    }

    return localStorage.setItem('authUser', user)
  }

  const getPersistedToken = async () => {
    return localStorage.getItem('authAccessToken')
  }

  const setPersistedToken = async (token) => {
    setToken(token)
    return localStorage.setItem('authAccessToken', token)
  }

  const getPersistedDecodedToken = async () => {
    const decodedToken = await localStorage.getItem('authDecodedAccessToken')

    if (isString(decodedToken)) {
      return JSON.parse(decodedToken)
    }

    return decodedToken
  }

  const setPersistedDecodedToken = async (decodedToken) => {
    setDecodedToken(decodedToken)
    setPermissions(decodedToken.permissions)
    if (isObject(decodedToken)) {
      localStorage.setItem('permissions', JSON.stringify(decodedToken.permissions))
      return localStorage.setItem('authDecodedAccessToken', JSON.stringify(decodedToken))
    }

    return localStorage.setItem('authDecodedAccessToken', decodedToken)
  }

  const logout = async () => {
    setLoading(true);
    setIsAuthenticated(false);
    setUser(null);
    setDecodedToken(null);
    setLoading(false);

    localStorage.clear()
  };

  const handleLoginResponse = (authToken, authUser) => {

    const decodedToken = jwtDecode(authToken);

    setPersistedToken(authToken)
    setPersistedUser(authUser)
    setPersistedDecodedToken(decodedToken)
    setIsAuthenticated(true)
  }

  return (
    <Auth0Context.Provider
      value={{
        isAuthenticated,
        user,
        loading,
        handleLoginResponse,
        token,
        permissions,
        decodedToken,
        setPersistedToken,
        logout
      }}
    >
      {children}
    </Auth0Context.Provider>
  );
};
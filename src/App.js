import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { useAuth0 } from './components/Auth/Auth'
import PrivateRoute from './components/Auth/PrivateRoute'
import AuthRoute from './components/Auth/AuthRoute'
import Admin from './components/Admin/Admin'
import Login from './components/Auth/Login'
import GlobalLoading from './components/Utility/GlobalLoading'

const App = () => {

  const { loading } = useAuth0();

  if (loading) return <GlobalLoading />

  return (
    <Router>
      <Switch>
        <PrivateRoute path="/" exact component={Admin} />
        <Route path="/public" exact render={() => <p>public</p>} />
        <AuthRoute path="/login" exact component={Login} />
        <PrivateRoute component={Admin} />
      </Switch>
    </Router>
  );
}

export default App;

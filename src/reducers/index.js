import {
  REQUEST_PAGES,
  RECEIVE_PAGES
} from './actions'

const initialState = {
  isLoadingRequest: false,
  hasLoadedPages: false,
  pages: []
}

const reducer = (state = initialState, action) => {

  switch (action.type) {

    case RECEIVE_PAGES:
      return Object.assign({}, state, {
        isLoadingRequest: false,
        hasLoadedPages: true,
        pages: [
          ...state.pages,
          ...action.pages
        ]
      })
    case REQUEST_PAGES:
      return Object.assign({}, state, {
        isLoadingRequest: true
      })
    default:
      return state
  }
}

export default reducer

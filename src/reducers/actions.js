import * as contentful from 'contentful'

const contentfulClient = contentful.createClient({
  space: `kuozvi4wnq42`,
  accessToken: `NUurf2kJEWp8iCpKi3C03tDVXXD-Q5dH35J8011Dius`
})

export const REQUEST_PAGES = `REQUEST_PAGES`
export const RECEIVE_PAGES = `RECEIVE_PAGES`

const requestPages = (subreddit) => ({
  type: REQUEST_PAGES,
  subreddit
})

const receivePages = (pages) => ({
  type: RECEIVE_PAGES,
  pages
})

export const fetchPages = () => {

  return async (dispatch) => {

    dispatch(requestPages())

    try {

      const response = await contentfulClient.getEntries()

      return dispatch(receivePages(response.items))
    } catch(err) {

      console.log(`Error requesting getEntries() `, err)
    }
  }
}

import axios from 'axios';
import qs from 'qs';
import { omitBy, isNil } from 'lodash';

class ApiWrapper {
  constructor ({ token, baseUrl }) {

    this.token = null;
    this.baseUrl = null;
    this.httpInstance = null

    this.createHttpInstance()
    this.setToken(token)
    this.setBaseUrl(baseUrl)
  }

  createHttpInstance () {
    this.httpInstance = axios.create({
      baseURL: this.baseUrl,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${this.token}`
      }
    });
  }

  setBaseUrl (baseUrl = 'localhost:3000') {
    this.httpInstance.defaults.baseURL = baseUrl
    this.baseUrl = baseUrl
  }

  setToken (token) {
    this.token = token
    this.httpInstance.defaults.headers.Authorization = `Bearer ${this.token}`
  }

  getEstacion (estacionId) {
    return this.httpInstance.get(`/estacion/${estacionId}`)
  }

  getEstaciones ({ search }) {

    const queryString = qs.stringify(omitBy({
      search
    }, isNil))

    return this.httpInstance.get(`/estacion${queryString}`)
  }
}

export default ApiWrapper

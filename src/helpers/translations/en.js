const translation = {
  translation: {
    station: 'station',
    programs: 'programs',
    mensajito: 'mensajito',
    'user management': 'user management',
    account: 'account',
    info: 'info',
    billing: 'billing',
    settings: 'settings'
  }
}

export default translation
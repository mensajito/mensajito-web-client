const translation = {
  translation: {
    station: 'estacíon',
    programs: 'programas',
    mensajito: 'mensajito',
    'user management': 'usuários',
    account: 'cuenta',
    info: 'info',
    billing: 'estado de cuenta',
    settings: 'configuracíon',
  }
}

export default translation
import ApiWrapperContructor from './api-wrapper-library';

const ApiWrapper = new ApiWrapperContructor({
  baseUrl: process.env.REACT_APP_API_BASE_URL
})

export default ApiWrapper
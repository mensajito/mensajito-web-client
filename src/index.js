import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter } from 'react-router-dom';
import '../node_modules/antd/dist/antd.less';
import './styles/index.css';
import App from './App';
import './helpers/translation';
import { Auth0Provider } from './components/Auth/Auth';

ReactDOM.render(
  <BrowserRouter>
    <Auth0Provider
      domain={process.env.REACT_APP_AUTH0_DOMAIN}
      client_id={process.env.REACT_APP_AUTH0_CLIENT_ID}
      audience={process.env.REACT_APP_AUTH0_AUDIENCE}
    >
      <App />
    </Auth0Provider>
  </BrowserRouter>,
  document.getElementById(`root`)
)

serviceWorker.unregister()

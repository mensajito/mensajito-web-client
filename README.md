# Mensajito Web Client
Este proyecto usa [Create React App](https://github.com/facebook/create-react-app) y [Contentful](https://github.com/contentful/contentful.js) para sus datos

## Cómo Empezar
```git clone git@gitlab.com:gavinfoster/mensajito-web-client.git```

```cd mensajito-web-client```

```npm install```

```npm start```

```open localhost:30000```

### Pruebas

```npm test```

### Backend / API

Portal de Dev: [Pantheon.io](https://dashboard.pantheon.io/sites/2f74030a-c3f5-4888-b917-ce1a237f64f0)
